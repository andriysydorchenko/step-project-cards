const token = localStorage.getItem('token');
const req = axios.create({
    baseURL: 'http://cards.danit.com.ua',
    headers: {
         Authorization: `Bearer ${token}`
    }
});
export {req, token}

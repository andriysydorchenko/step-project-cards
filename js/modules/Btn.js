// класс кнопок - который создает обект кнопки, с методом рендер котрый возвращает дом елемент кнопки.
// Пример такого класса в класной работе за 17.04.2020-es6-class файл exercise-3.
// Сюда нужно будет передавать полностью все настройки нашей кнопки (классы, ид, текст в кнопке, функция обработчик собитий)

//Автор Чубин Дима

class Button {
    constructor({text="", id="", classList="", activeClass=""}){
        this.text = text;
        this.id = id;
        this.classList = [...classList]
        this.activeClass = activeClass;
        this.Btn = null;
    }
    render(){
        this.Btn = document.createElement('button');
        const {Btn} = this;
        Btn.classList = this.classList.join(' ');
        Btn.id = this.id;
        Btn.textContent = this.text;
        return Btn;
    }
}

export default Button;

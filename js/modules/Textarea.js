class Textarea{
    constructor({name='', placeholder='', classes='', id='', labelText, value = "", required=false}){
        this.name = name;
        this.placeholder = placeholder;
        this.classes = classes;
        this.id = id;
        this.value = value;
        this.labelText = labelText;
        this.required = required;
    }
    render(){
        const elem = document.createElement('div');
        elem.className ='form-group';
        elem.insertAdjacentHTML('beforeend', `
            <label>${this.labelText}</label>
            <textarea class="${this.classes}" name="${this.name}" id="${this.id}" placeholder="${this.placeholder}" ${(this.required)?'required':''}></textarea>
        `);
        return elem;
    }
}
export default Textarea
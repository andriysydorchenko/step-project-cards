// класс input - который создает обьект интупута, с методом рендер котрый возвращает дом елемент инпута.
// Он наподобие кнопки.
// Автор: Сидорченко Андрей



class Input {
    constructor({value ="", type, id ="", classes, name="", placeholder="", required = false, labelText}){
        this.value = value;
        this.name = name;
        this.id = id;
        this.type = type;
        this.classes = classes;
        this.placeholder = placeholder;
        this.required = required;
        this.labelText = labelText;
        this.inputDom = null;
    }
    render(){
        this.formGroup = document.createElement("div");
        const {formGroup} = this;
        formGroup.className = "form-group";

        if (this.labelText) {
            const label = document.createElement("label");
            label.innerText = this.labelText;
            formGroup.append(label);
        }

        this.inputDom = document.createElement("input");
        const {inputDom} = this;
        inputDom.className = this.classes;
        inputDom.id = this.id;
        inputDom.value = this.value;
        inputDom.type = this.type;
        inputDom.name = this.name;
        inputDom.placeholder = this.placeholder;
        inputDom.required = this.required;

        formGroup.append(inputDom);
        return formGroup;
    }
}
export default Input;
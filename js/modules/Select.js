// Автор: Сидорченко Андрей
// for the options you need to gave the array of objects with the property: value, selected, text
// For example: {
//                 value: "dentist",
//                 text: "Стоматолог",
//                 selected: true,
//              }
class Select {
    constructor({options, classes ="", id ="", name = "",labelText, target="", required = false}) {
        this.options = options;
        this.name = name;
        this.classes = classes;
        this.id = id;
        this.target = target;
        this.labelText = labelText;
        this.required = required;
        this.formGroup = null;
    }
    render(){
        this.formGroup = document.createElement("div");
        const {formGroup} = this;
        formGroup.className = "form-group";

        const label = document.createElement("label");
        label.innerText = this.labelText;
        formGroup.append(label);

        const selectDom = document.createElement("select");
        selectDom.className = this.classes;
        selectDom.id = this.id;
        selectDom.name = this.name;
        selectDom.dataset.target =this.target;
        selectDom.required = this.required;
        const options = this.options.map(option =>{
            if (option.selected) {
                return `<option value = ${option.value} selected = ${option.selected}>${option.text}</option>`;
            }
            else return `<option value = ${option.value} >${option.text}</option>`;

        }).join('');
        selectDom.insertAdjacentHTML("beforeend", options);

        formGroup.append(selectDom);

        return this.formGroup;
    }

}
export default Select;
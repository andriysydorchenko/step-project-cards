// Автор: Сидорченко Андрей


function serializeJSON (form) {
    const body = {};
    form.querySelectorAll('input,  select, textarea').forEach(element => {
        const name = element.getAttribute('name');
        if(name && name !== "submit") {
            body[name] = element.value;
        }
    });
    return body;
}

export {serializeJSON};
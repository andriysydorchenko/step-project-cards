import {Visit, VisitCardiologist, VisitDentist, VisitTherapist} from "../classes/index.js"
function createVisit(obj){
    if(obj.doctor === 'Кардиолог'){
        return new VisitCardiologist(obj);
    } else if(obj.doctor === 'Стоматолог'){
        return new VisitDentist(obj);
    } else if(obj.doctor === 'Терапевт'){
        return new VisitTherapist(obj);
    }
}

export {createVisit};
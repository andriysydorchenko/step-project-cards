
// Автор: Сидорченко Андрей
const propertyForm = {
    getInputProps({date, fullName, title}){
        return [{
            labelText: "Дата приема",
            type: "text",
            classes: "form-control",
            name: "date",
            value: date,
            required: true
        }, {
            labelText: "ФИО пациента",
            type: "text",
            classes: "form-control",
            name: "fullName",
            value: fullName,
            required: true
        }, {
            labelText: "Цель визита пациента",
            type: "text",
            classes: "form-control",
            name: "title",
            value: title,
            required: true
        }]
    },
    getSelectDoctor({doctor}) {
        const options = [{
            value: "",
            text: "Виберете доктора",
        },{
            value: "Терапевт",
            text: "Терапевт",
        }, {
            value: "Кардиолог",
            text: "Кардиолог",
        }, {
            value: "Стоматолог",
            text: "Стоматолог",
        }];


        if(doctor){
            let numDoc;
            switch (doctor) {
                case "Терапевт": {
                    numDoc = 1;
                    break;
                }
                case "Кардиолог": {
                    numDoc = 2;
                    break;
                }
                case "Стоматолог": {
                    numDoc = 3;
                    break;
                }
            }
            options[numDoc].selected = true;
        }


        const selectProp = {
            labelText: "Виберете доктора",
            classes: "form-control",
            name: "doctor",
            target: ".additional-fields",
            options: options,
            required: true
        };
        return selectProp

    },
    selectUrgencyProp: {
        labelText: "Виберете срочность приема",
        classes: "form-control",
        name: "urgency",
        options: [{
            value: "Обычная",
            text: "Обычная"
        }, {
            value: "Приоритетная",
            text: "Приоритетная"
        }, {
            value: "Неотложная",
            text: "Неотложная"
        }],
        required: true
    },
    getTextareaProp(description = "") {
        const textareaProp = {
            labelText: "Краткое описание опысание болезни",
            classes:"form-control",
            name:"description",
            value: description
        };
        return textareaProp

    },
    submitBtnProp: {
        type: "submit",
        name: "submit",
        value: "Добавить запись",
        classes: "form-add-visit",
        id: "add-visit"
    }

};

function getAdditionalFieldProp({doctor,age="", pressure="", heartDiseases="" , bodyMassIndex="", lastVisit=""}){
    const additionalFieldProp = {
        Терапевт: [{
            labelText: "Возраст пациента",
            type: "text",
            classes: "form-control",
            name: "age",
            value: age,
            required: true
        }],
        Кардиолог: [{
            labelText: "Возраст пациента",
            type: "text",
            classes: "form-control",
            name: "age",
            value: age,
            required: true
        }, {
            labelText: "Давление пациента",
            type: "text",
            classes: "form-control",
            name: "pressure",
            value: pressure,
            required: true
        }, {
            labelText: "Индекс массы-тела пациента",
            type: "text",
            classes: "form-control",
            name: "bodyMassIndex",
            value: bodyMassIndex,
            required: true
        }, {
            labelText: "Болезни сердца пациента",
            type: "text",
            classes: "form-control",
            name: "heartDiseases",
            value: heartDiseases,
            required: true
        }],
        Стоматолог: [{
            labelText: "Дата последнего визита",
            type: "text",
            classes: "form-control",
            name: "lastVisit",
            value: lastVisit,
            required: true
        }]
    };
    return additionalFieldProp[doctor]
}

export {getAdditionalFieldProp, propertyForm};
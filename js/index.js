

// Пример работы с инпутом

// import {Input} from "./modules/index.js";
// const input = {
//     type: "password",
//     value: "asdf",
// };
// const newInput = new Input(input);
// console.log(newInput);
// const card = document.querySelector(".card");
// card.append(newInput.render());


// Пример работы селекта
// import {Select} from "./modules/index.js";
// const selectProp = {
//     options: [{
//         value: "dantist",
//         text: "дантист"
//     }, {
//         value: "therapy",
//         text: "терапевт"
//     }, {
//         value: "cardiology",
//         text: "кардиолог"
//     }],
//     id: "doctorCategory",
//     classes: "form-control",
//     labelText: "Виберете доктора"
// }
// const select = new Select(selectProp);
// console.log(select);
// const card = document.querySelector(".card");
// card.append(select.render());

//
//
// import {CardForm} from "./classes/index.js";
//
// const formCardProp = {
//    classes: "form"
// };
// const formCard = new CardForm(formCardProp);
//
// const card = document.querySelector(".modal-content");
// card.append(formCard.render());


//         modal window


import {Modal} from "./classes/index.js";
import {req, token} from "./propAxioRequest/propAxioRequest.js";
import {Visit, VisitCardiologist, VisitDentist, VisitTherapist} from "./classes/index.js";
import {createVisit, serializeJSON} from './functions/index.js'

// Автор: Чубин Дмитрий
const ModalAuthorisattionProp = {
     className: "modal",
     modalContent:"LoginForm"
};

document.getElementById('autorisationBtn').addEventListener('click', function (e) {
    e.preventDefault();
    const modalWindow = new Modal(ModalAuthorisattionProp);
    document.querySelector('body').prepend(modalWindow.render());
});

const ModalCardProp = {
     className: "modal",
     modalContent:"CardForm"
};

document.getElementById('createVisitBtn').addEventListener('click', function (e) {
    e.preventDefault();
    const modalWindow = new Modal(ModalCardProp);
    document.querySelector('body').prepend(modalWindow.render());
});


// Запрос на подгрузкку уже готовых карточек
// Автор: Сидорченко Андрей
const board = document.querySelector(".visit-board");
const request = req.get('/cards');
request.then(({data}) => {
    data.forEach(cardData => {
        const card = createVisit(cardData);
        board.append(card.render());
    })
});

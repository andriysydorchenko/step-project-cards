// создает екземпляр класс Форми(обект форма). Этот обьект будет вставляться в модальное окно.
// У него есть три дочерних класса: CardForm, LoginForm, FilterForm (с этим будем в последнию очередь разбираться).
// Их будем наполнять компонентами: инпуты. селекты, текстареал и.д.
import {Input, Select, Btn, Textarea} from "../modules/index.js";
import {propertyForm, emailInputProps, passwordImputProps, authorisationBtn, getAdditionalFieldProp} from "../templates_and_property/index.js";

import {serializeJSON} from "../functions/index.js";
import {req, token} from "../propAxioRequest/propAxioRequest.js";
import {Visit, VisitCardiologist, VisitDentist, VisitTherapist} from "../classes/index.js"
import {createVisit} from "../functions/index.js";

// Автор: Сидорченко Андрей
// for the options you need to gave the array of objects with the property: value, selected, text
// For example: {
//                 value: "dentist",
//                 text: "Стоматолог",
//                 selected: true,
//              }
class Form {
    constructor({classes, id =""}){
        this.classes = classes;
        this.id = id;
        this.formDom = null;
    }

    render() {
        this.formDom = document.createElement("form");
        const {formDom} = this;
        formDom.className = this.classes;
        formDom.id = this.id;
    }
}

// CLASS FOR CREATE FORM AUTHORISATION
//    Автор: Чубин Дмитрий

class LoginForm extends Form{
    static messages = {
    login: {
        "Success": "Вы успешно авторизированы",
        "Error": "Неверный пароль"
    }
};
     constructor({card, ...args}){
        super(args);
    }
    render() {
        super.render();
        const {formDom} = this;
        const emailInput = new Input(emailInputProps);
        const passInput = new Input(passwordImputProps);
        const btn = new Btn(authorisationBtn);
        formDom.append(emailInput.render());
        formDom.append(passInput.render());
        formDom.append(btn.render());
        formDom.addEventListener('submit',  this.getAuthorisation.bind(this));
        return formDom
    }
    getAuthorisation(event){
         event.preventDefault();
         const {formDom} = this;
         const body = serializeJSON(formDom);
         const request = req.post('/login', body);
         request.then(({data}) => {
          if(data.status === "Success"){
                localStorage.setItem("token", data.token);
              formDom.closest('div').remove();
              document.getElementById('autorisationBtn').classList.add('hide');
              document.getElementById('createVisitBtn').classList.remove('hide');
          }

           formDom.insertAdjacentHTML('afterend', `<p class="error">${LoginForm.messages.login[data.status]}</p>`);
          }).catch(error => console.log(error));
          document.querySelector('.error').remove();
}
    }



// CLASS FOR CREATE AND EDIT FORM CARD VISIT
// Автор: Сидорченко Андрей
class CardForm extends Form{
    constructor({card, ...args}){
        super(args);
        this.card = card;
    }
    render() {
        super.render();
        const {formDom} = this;
        let prop = {};
        if(this.card) {
            prop = this.card;
            formDom.addEventListener("submit", this.editCard.bind(this));
        } else {
            prop = {
                doctor:"",
                fullName:"",
                title: "",
                description: "",
                urgency: "",

            };
            formDom.addEventListener("submit", this.createCard.bind(this));
        }


        propertyForm.getInputProps(prop).map(prop => {
            const input = new Input(prop);
            formDom.append(input.render());
        });

        // SELECTOR FOR DOCTOR
        const divSelectDoctor = new Select(propertyForm.getSelectDoctor(prop));
        const divSelectDoctorDom = divSelectDoctor.render();
        const selectDoctorDom = divSelectDoctorDom.querySelector("select");
        selectDoctorDom.addEventListener("change", this.showAdditionalFields);
        formDom.append(divSelectDoctorDom);


            // SELECTOR FOR URGENCY
        const selectUrgency = new Select(propertyForm.selectUrgencyProp);
        formDom.append(selectUrgency.render());

        // ADDITIONAL FIELDS
        const additionalField = document.createElement("div");
        additionalField.className = "additional-fields";
        formDom.append(additionalField);

        const textarea = new Textarea(propertyForm.getTextareaProp(prop));
        formDom.append(textarea.render());


        // ADD SUBMIT BTN
        const divBtnClass = new Input(propertyForm.submitBtnProp);
        const divBtn = divBtnClass.render();
        formDom.append(divBtn);

        if(this.card) {
            prop = this.card;
            this.showAdditionalFieldsCard(prop);
        }

        return formDom;
    }
    showAdditionalFields(){
        this.children[0].setAttribute('disabled', 'true');
        const {target} = this.dataset;
        const obj = {
            doctor: this.value
        };
        const inputProps = getAdditionalFieldProp(obj);
        const additionalFields = this.closest('form').querySelector(target);
        additionalFields.innerHTML = "";
        inputProps.map(prop => {
            const input = new Input(prop);
            additionalFields.append(input.render());
        })
    }
    showAdditionalFieldsCard(card){
        const doctor = card.doctor;
        const inputProps = getAdditionalFieldProp(card);

        const selectDoctorDom = this.formDom.querySelector("select");
        const {target} = selectDoctorDom.dataset;
        const additionalFields = selectDoctorDom.closest('form').querySelector(target);
        additionalFields.innerHTML = "";
        inputProps.map(prop => {
            const input = new Input(prop);
            additionalFields.append(input.render());
        })

    }
    createCard(event){
        event.preventDefault();
        const {formDom} = this;
        const body = serializeJSON(formDom);

        const request = req.post('/cards', body);
        request.then(({data}) => {
            if(data.id){
                // console.log(data);
                const board = document.querySelector(".visit-board");
                // board.innerHTML = "";

                const card = createVisit(data);
                board.append(card.render());

                const modal = this.formDom.closest(".modal");
                modal.remove();
                // console.log(modal);

                // const product = new Notebook(data);
                // document.getElementById('productBoard').append(product.render());
                // products.push();
            }
        });


    }
    editCard(event){
        event.preventDefault();
        const {formDom} = this;
        const body = serializeJSON(formDom);
        const request = req.put(`/cards/${this.card.id}`, body);
        request.then(({data}) => {
            if (data.id) {
                const board = document.querySelector(".visit-board");

                const newCard = createVisit(data);
                this.card.card.remove();
                board.append(newCard.render());
                const modal = this.formDom.closest(".modal");
                modal.remove();
            }
        })
    }


}


export {Form, CardForm, LoginForm}

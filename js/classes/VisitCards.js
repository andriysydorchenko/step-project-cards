// класс карточки Visit  и его дочернии классы VisitDentist, VisitCardiologist, VisitTherapist.
// Выполняет функцию создания дом елемента карточки, наполняет компанентами, накидывет обработчик собитий и т.д.
import {CardForm} from "./Forms.js";

class Visit{
    constructor(doctor, fullName, title, description ="", urgency, id){
        this.doctor = doctor;
        this.fullName = fullName;
        this.title = title;
        this.description = description;
        this.urgency = urgency;
        this.id = id;
        this.card = null;
    }

    render(){
        this.card = document.createElement('div');
        const {card} = this;
        card.classList.add('card');
        card.insertAdjacentHTML('beforeend', `
                    <p class="card-text" name='fullName'>${this.fullName}</p>
                    <p class="card-text" name='doctor'>к ${this.doctor}у</p>
                    <p class="card-show_more" id="card-show_more">Показать больше</p>
                    <i class="fas fa-times cross" id="card-delete"></i>
                    <i class="fas fa-edit card-edit" id="card-edit"></i>
        `);
        const visitBoard = document.getElementById('visit-board');
        card.querySelector('#card-show_more').addEventListener('click', () => {this.showMore(card)});
        card.querySelector('#card-delete').addEventListener('click', () => {this.cardDelete(card)});
        card.querySelector('#card-edit').addEventListener('click', () => {this.cardEdit()});
        return card;
    }
    
    showMore(card){
        card.querySelector('#card-show_more').remove();
        card.insertAdjacentHTML('beforeend', `
            <p class="card-text" name='title'>${this.title}</p>
            <p class="card-text" name='description'>${this.description}</p>
            <p class="card-text" name='urgency'>${this.urgency} срочность</p>
        `);
    }
    
    cardDelete(card){
        //вызивает форму подтверждения пароля, которая вобщем делает все остальное
    }
    cardEdit(card){
        //вызывает форму редактирования, которая вобщем все остальное делает
        const formAuthorisationProp = {
            classes: "form",
            card: this
        };
        const form = new CardForm(formAuthorisationProp);
        document.querySelector("body").insertAdjacentHTML("afterend", `
                 <div class="modal">
                    <div class="modal-content">
                    </div>
                </div>`);
        const modal = document.querySelector(".modal-content");
        modal.append(form.render());
    }
}

class VisitDentist extends Visit{
    constructor({doctor, fullName, title, description, urgency, id, lastVisit}){
        super(doctor, fullName, title, description, urgency, id);
        this.lastVisit = lastVisit;
    }
    showMore(card){
        super.showMore(card);
        card.insertAdjacentHTML('beforeend', `
            <p class="card-text" name='lastVisit'>Последний визит был ${this.lastVisit}</p>
        `);
    }
}

class VisitCardiologist extends Visit{
    constructor({doctor, fullName, title, description, urgency, id, pressure, bodyMassIndex, heartDiseases}){
        super(doctor, fullName, title, description, urgency, id);
        this.pressure = pressure;
        this.bodyMassIndex = bodyMassIndex;
        this.heartDiseases = heartDiseases;
    }
    showMore(card){
        super.showMore(card);
        card.insertAdjacentHTML('beforeend', `
            <p class="card-text" name='pressure'>Давление: ${this.pressure} мм рт. ст.</p>
            <p class="card-text" name='bodyMassIndex'>Индекс массы-тела: ${this.bodyMassIndex}</p>
            <p class="card-text" name='heartDiseases'>Частота сердцебиения: ${this.heartDiseases} уд./мин</p>
        `);
    }
}

class VisitTherapist extends Visit{
    constructor({doctor, fullName, title, description, urgency, id, age}){
        super(doctor, fullName, title, description, urgency, id);
        this.age = age;
    }
    showMore(card){
        super.showMore(card);
        card.insertAdjacentHTML('beforeend', `
            <p class="card-text" name='age'>Возраст: ${this.age} лет</p>
        `);
    }
}

export {Visit, VisitCardiologist, VisitDentist, VisitTherapist};
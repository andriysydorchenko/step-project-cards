//класс Modal, который будет создавать объект, описывающий каркас всплывающего окна.
// Пример в класной рабооте за 21.04.2020-es6-class-practice файл exercise-2.
// Потом его будем наполнять другими компонентами типа: конопок, форма с инпутами и т.д.


import {LoginForm, CardForm} from "./index.js";

const formAuthorisationProp = {
    classes: "form"
};

class Modal {
    constructor({className, id="", modalContent=""}) {
        this.className = className;
        this.id = id;
        this.modalContent = modalContent;
        this.modalWindow = null;
    }
    render() {
                this.modalWindow = document.createElement('div');
                let {modalWindow} = this;
                let content = null
                modalWindow.className = this.className;
                if(this.modalContent == "LoginForm"){
                      content = new LoginForm(formAuthorisationProp);
                   }
                   if(this.modalContent == "CardForm"){
                      content = new CardForm(formAuthorisationProp);
                   }
                modalWindow.innerHTML =`
                    <dic class="modal-content">
                          <span class="cross">&times;</span>
                    </div>
                `;
                modalWindow.children[0].append(content.render());
                const confirmBtn = modalWindow.querySelector("#configBtn");
                modalWindow.addEventListener('click', this.closeModal.bind(this));
                return modalWindow;
            }
        closeModal({target}){
            if(target.classList.contains("cross")) {
                 target.closest('div').remove()
            }
        }
        }

export {Modal}
